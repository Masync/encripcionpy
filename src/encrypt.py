import os

KEYS = {
    'a': 'w',
    'b': 'E',
    'c': 'x',
    'd': '1',
    'e': 'a',
    'f': 't',
    'g': '0',
    'h': 'C',
    'i': 'b',
    'j': '!',
    'k': 'z',
    'l': '8',
    'm': 'M',
    'n': 'I',
    'o': 'd',
    'p': '.',
    'q': 'U',
    'r': 'Y',
    's': 'i',
    't': '3',
    'u': ',',
    'v': 'J',
    'w': 'N',
    'x': 'f',
    'y': 'm',
    'z': 'W',
    'A': 'G',
    'B': 'S',
    'C': 'j',
    'D': 'n',
    'E': 's',
    'F': 'Q',
    'G': 'o',
    'H': 'e',
    'I': 'u',
    'J': 'g',
    'K': '2',
    'L': '9',
    'M': 'A',
    'N': '5',
    'O': '4',
    'P': '?',
    'Q': 'c',
    'R': 'r',
    'S': 'O',
    'T': 'P',
    'U': 'h',
    'V': '6',
    'W': 'q',
    'X': 'H',
    'Y': 'R',
    'Z': 'l',
    '0': 'k',
    '1': '7',
    '2': 'X',
    '3': 'L',
    '4': 'p',
    '5': 'v',
    '6': 'T',
    '7': 'V',
    '8': 'y',
    '9': 'K',
    '.': 'Z',
    ',': 'D',
    '?': 'F',
    '!': 'B',
    ' ': '&',
}

def encrypt(P_word:str):
    os.system('cls')
    word = P_word.split(' ')
    encrypt_message_arr = []
    for idx in word:
        msg = ' '
        for letter_idx in idx:
            
            encrypt_message_arr += KEYS[letter_idx]
        encrypt_message_arr.append(msg)

    return print(''.join(encrypt_message_arr ))

def decrypt(P_word:str):

    os.system('cls')

    word = P_word.split(' ')
    encrypt_message_arr = []
    for idx in word:
        msg = ' '
        for letter_idx in idx:
            
           for idx_mssg, value_mssg in KEYS.items():
               if value_mssg == letter_idx:
                   msg += idx_mssg
        encrypt_message_arr.append(msg)

    return print(''.join(encrypt_message_arr ))




def run():
    option = input ( '''
        [E]ncrypt
        [D]ecrypt
    ''')

    if (option == 'E' or option == 'e'):
        message = input ('Ingrese Mensaje:')
        encrypt(message)

    elif (option == 'D' or option == 'd' ):
        message = input ('Ingrese Mensaje:')
        decrypt(message)
    else:
        print('Valor no reconocido')
        exit()



if __name__ == "__main__":
        print('''
▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
█░▄▄█░▄▄▀█▀▄▀█░▄▄▀█░██░█▀▄▄▀█▄░▄███░▄▄▀█▀▄▄▀█▀▄▄▀
█░▄▄█░██░█░█▀█░▀▀▄█░▀▀░█░▀▀░██░████░▀▀░█░▀▀░█░▀▀░
█▄▄▄█▄██▄██▄██▄█▄▄█▀▀▀▄█░█████▄████▄██▄█░████░███
▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀

         ''')
        run()